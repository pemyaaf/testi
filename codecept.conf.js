const { setHeadlessWhen, setCommonPlugins } = require('@codeceptjs/configure');
// eslint-disable-next-line no-undef
const {config} = require("@codeceptjs/examples/codecept.conf");
// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

// enable all common plugins https://github.com/codeceptjs/configure#setcommonplugins
setCommonPlugins();

/** @type {CodeceptJS.MainConfig} */
exports.config = {
  tests: './*_test.js',
  output: './output',
  helpers: {
    Playwright: {
      url: 'localhost',
      browser: 'chromium',
    },
  },
  include: {
    I: './steps_file.js'
  },
  name: 'testci',
  mocha: {
    reporterOptions: {
      "reportDir": "output",
      "reportFilename": "report.html",
    }
  }
}


if (process.profile === "test") {
  config.helpers.Playwright.url = process.env.SELENIUM_STANDALONE_CHROME_PORT_4444_TCP_ADDR;
}
